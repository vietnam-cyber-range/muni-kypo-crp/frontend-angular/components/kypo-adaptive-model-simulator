### 16.0.0 Update to Angular 16.
* e9fac21 -- [CI/CD] Update packages.json version based on GitLab tag.
* e602d7b -- Merge branch '10-update-to-angular-16' into 'master'
* d0a5dd8 -- Update to Angular 16, update local issuer to keycloak
### 15.0.1 Resolve Angular Material migration.
* e515adc -- [CI/CD] Update packages.json version based on GitLab tag.
* 828682e -- Merge branch 'resolve-Angular-15-material' into 'master'
* 2807c52 -- Resolve new Material styling
### 15.0.0 Update to Angular 15.
* 5324787 -- [CI/CD] Update packages.json version based on GitLab tag.
* 3e2eaa8 -- Merge branch '9-update-to-angular-15' into 'master'
* b3a3b34 -- Update to Angular 15
### 14.1.1 Fix task stepper for post-training tool.
* 7c3ec53 -- [CI/CD] Update packages.json version based on GitLab tag.
* 4d53993 -- Merge branch '8-fix-task-stepper-for-instance-simulator' into 'master'
* 21e898b -- Resolve "Fix task stepper for instance simulator"
### 14.1.0 Add support for an access phase for the pre-training tool. Fix computing of a trainees' performance.
* 8166c66 -- [CI/CD] Update packages.json version based on GitLab tag.
* d87ba21 -- Merge branch '7-fix-the-computation-of-the-trainees-path' into 'master'
* d27802d -- Resolve "Fix the computation of the trainees' path"
### 14.0.1 Remove unused css and move object creation to mapper.
* 52762e5 -- [CI/CD] Update packages.json version based on GitLab tag.
* 22d6dde -- Merge branch '4-refactor-model-simulator' into 'master'
* 77c31f1 -- Resolve "Refactor model simulator"
### 14.0.0 Angular 14 version of the tool.
